const Todo = require('../models/Todo');

// @desc      Get all todos
// @route     GET /api/v1/todos
// @access    Public
exports.getTodos = async (req, res, next) => {
  try {
    const todos = await Todo.find();

    return res.status(200).json({
      success: true,
      count: todos.length,
      data: todos,
    });
  } catch (err) {
    return res.status(500).json({
      success: false,
      error: 'Server Error',
    });
  }
};

// @desc      Update Todo
// @route     PUT /api/v1/todos
// @access    Public
exports.updateTodo = async (req, res, next) => {
  try {
    const { done } = await Todo.findById(req.params.id);
    const todo = await Todo.updateOne({ _id: req.params.id }, { done: !done });
    return res.status(200).json({
      success: true,
      data: todo,
    });
  } catch (err) {
    return res.status(500).json({
      success: false,
      error: 'Server Error',
    });
  }
};

// @desc      Add todo
// @route     POST /api/v1/todos
// @access    Public
exports.addTodo = async (req, res, next) => {
  try {
    const todo = await Todo.create(req.body);
    return res.status(201).json({
      success: true,
      data: todo,
    });
  } catch (err) {
    return res.status(400).json({
      success: false,
      error: err.message,
    });
  }
};

// @desc      Delete todo
// @route     DELETE /api/v1/todos/:id
// @access    Public
exports.deleteTodo = async (req, res, next) => {
  try {
    const todo = await Todo.findById(req.params.id);

    if (!todo) {
      return res.status(404).json({
        success: false,
        error: 'No todo found',
      });
    }

    await todo.remove();
    return res.status(200).json({
      success: true,
      data: {},
    });
  } catch (err) {
    return res.status(500).json({
      success: false,
      error: 'Server Error',
    });
  }
};
