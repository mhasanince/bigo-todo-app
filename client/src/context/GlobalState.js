import React, { createContext, useReducer } from 'react';
import AppReducer from './AppReducer';
import axios from 'axios';

const initialState = {
  todos: [],
  error: null,
  loading: true,
};

export const GlobalContext = createContext(initialState);

export const GlobalProvider = ({ children }) => {
  const [state, dispatch] = useReducer(AppReducer, initialState);

  // ACTIONS
  const getTodos = async () => {
    try {
      const res = await axios.get('/api/v1/todos');
      dispatch({
        type: 'GET_TODOS',
        payload: res.data.data,
      });
    } catch (err) {
      dispatch({
        type: 'TODO_ERROR',
        payload: err.response.data.error,
      });
    }
  };

  const addTodo = async (todo) => {
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    try {
      await axios.post('/api/v1/todos', todo, config);
      dispatch({
        type: 'ADD_TODO',
        payload: todo,
      });
    } catch (err) {
      dispatch({
        type: 'TODO_ERROR',
        payload: err.response.data.error,
      });
    }
  };

  const checkTodo = async (id) => {
    try {
      await axios.put(`/api/v1/todos/${id}`);
      dispatch({
        type: 'CHECK_TODO',
        payload: id,
      });
    } catch (err) {
      dispatch({
        type: 'TODO_ERROR',
        payload: err.response.data.error,
      });
    }
  };

  const deleteTodo = async (id) => {
    try {
      await axios.delete(`/api/v1/todos/${id}`);
      dispatch({
        type: 'DELETE_TODO',
        payload: id,
      });
    } catch (err) {
      dispatch({
        type: 'TODO_ERROR',
        payload: err.response.data.error,
      });
    }
  };

  return (
    <GlobalContext.Provider
      value={{
        todos: state.todos,
        error: state.error,
        loading: state.loading,
        getTodos,
        addTodo,
        deleteTodo,
        checkTodo,
      }}
    >
      {children}
    </GlobalContext.Provider>
  );
};
