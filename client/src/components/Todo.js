import React, { useContext } from 'react';
import { GlobalContext } from '../context/GlobalState';
import styles from './Todo.module.scss';

const Todo = ({ text, done, id }) => {
  const { checkTodo, deleteTodo } = useContext(GlobalContext);
  const handleCheck = () => {
    checkTodo(id);
  };
  const handleDelete = () => {
    deleteTodo(id);
  };
  return (
    <div className={`${styles['todo']}`} onClick={handleCheck}>
      <p className={`${done && styles['done']}`}>{text}</p>
      <button onClick={handleDelete}>Delete</button>
    </div>
  );
};

export default Todo;
