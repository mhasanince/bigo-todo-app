import React, { useEffect, useContext } from 'react';
import { GlobalContext } from '../context/GlobalState';
import TodoForm from './TodoForm';
import TodoList from './TodoList';
import styles from './TodoApp.module.scss';

const TodoApp = () => {
  const { getTodos } = useContext(GlobalContext);
  useEffect(() => {
    getTodos();
  }, []);
  return (
    <div className={styles['todo-app']}>
      <div className={styles['container']}>
        <h1 className={styles['header']}>
          Todo<span>App</span>
        </h1>
        <TodoForm />
        <TodoList />
      </div>
    </div>
  );
};

export default TodoApp;
