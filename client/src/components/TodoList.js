import React, { useContext } from 'react';
import { GlobalContext } from '../context/GlobalState';
import styles from './TodoList.module.scss';
import Todo from './Todo';

const TodoList = () => {
  const { todos } = useContext(GlobalContext);
  return (
    <div className={styles['todo-list']}>
      {todos.map((todo) => (
        <Todo key={todo._id} text={todo.text} done={todo.done} id={todo._id} />
      ))}
    </div>
  );
};

export default TodoList;
