import React, { useState, useContext } from 'react';
import { GlobalContext } from '../context/GlobalState';
import styles from './TodoForm.module.scss';

const TodoForm = () => {
  const [text, setText] = useState('');
  const { addTodo, getTodos } = useContext(GlobalContext);
  const handleClick = () => {
    const newTodo = {
      text: text,
    };
    addTodo(newTodo);
    setText('');
    getTodos();
  };
  return (
    <div className={styles['todo-form']}>
      <input
        type="text"
        value={text}
        onChange={(e) => setText(e.target.value)}
        placeholder="Add new todo"
      />
      <button onClick={handleClick}>Add</button>
    </div>
  );
};

export default TodoForm;
